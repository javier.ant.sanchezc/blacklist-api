package com.bootcamp.coders.blacklist_openapiv3.project.api.model;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.util.Assert;

import javax.validation.constraints.AssertTrue;
import java.time.LocalDate;

import static org.junit.jupiter.api.Assertions.*;

class CustomerInfoTest {
    CustomerInfo customerInfo;
    CustomerInfo customerInfo2;
    CustomerInfo customerInfo3;

    final LocalDate LOCAL_DATE = LocalDate.of(2019, 12, 17);

    @BeforeEach
    void setUp() {
        customerInfo = new CustomerInfo();
        customerInfo2 = new CustomerInfo();
        customerInfo3 = new CustomerInfo();


        customerInfo.setName("name");
        customerInfo.setDocument("123456789");
        customerInfo.setLastName("pepito perez");
        customerInfo.setCellphone("3156770860");
        customerInfo.setBirthDate(LOCAL_DATE);
        customerInfo.setDocumentIssueDate(LOCAL_DATE);
        customerInfo.setEmail("correo@correo.com");
        customerInfo.setDocumentType("CC");







    }

    @AfterEach
    void tearDown() {
    }

    @Test
    void getDocumentType() {
        assertEquals(customerInfo.getDocumentType(),"CC");
    }

    @Test
    void getDocument() {
        assertEquals(customerInfo.getDocument(),"123456789");
        System.out.println(customerInfo);
    }

    @Test
    void getName() {

        assertEquals(customerInfo.getName(),"name");
    }

    @Test
    void getLastName() {
        assertEquals(customerInfo.getLastName(),"pepito perez");
    }

    @Test
    void getDocumentIssueDate() {
        assertEquals(customerInfo.getDocumentIssueDate(),LOCAL_DATE);
    }

    @Test
    void getBirthDate() {
        assertEquals(customerInfo.getBirthDate(),LOCAL_DATE);
    }

    @Test
    void getEmail() {
        assertEquals(customerInfo.getEmail(),"correo@correo.com");
    }

    @Test
    void getCellphone() {
        assertEquals(customerInfo.getCellphone(),"3156770860");
    }


    String messageForFailedEqualsTest( String messagePrefix ) {
        String string1 = String.valueOf( customerInfo );
        String string2 = String.valueOf( customerInfo2 );
        return String.format( "%s for: <%s> and: <%s>", messagePrefix, string1, string2 );
    }

    @Test
    void testEquals() {
       // assertTrue(customerInfo.equals(customerInfo3)&&customerInfo3.equals(customerInfo));
        String message = messageForFailedEqualsTest( "Unequals test failed" );
        assertFalse(customerInfo.equals( customerInfo2 ), message);

    }


    @Test
    void testHashCode() {

        assertTrue(customerInfo2.hashCode()==customerInfo3.hashCode());


    }


}