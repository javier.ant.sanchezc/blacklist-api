package com.bootcamp.coders.blacklist_openapiv3.project.api.model;

import liquibase.pro.packaged.E;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class BlackListResponseTest {

    BlackListResponse blackListResponse;
    Error error;

    @BeforeEach
    void setUp() {
        blackListResponse = new BlackListResponse();
        List<Error> lista = new ArrayList<Error>();
        lista.add(error =new Error());
        blackListResponse.setErrores(lista);

    }

    @AfterEach
    void tearDown() {
    }

    @Test
    void data() {
    }

    @Test
    void getData() {
    }

    @Test
    void setData() {
    }

    @Test
    void errores() {
    }

    @Test
    void addErroresItem() {
    }

    @Test
    void getErrores() {
        List<Error> lista = new ArrayList<Error>();
        lista.add(error =new Error());
        blackListResponse.setErrores(lista);
        assertEquals(blackListResponse.getErrores(),lista);
    }

    @Test
    void setErrores() {
    }

    @Test
    void testEquals() {
    }

    @Test
    void testHashCode() {
    }

    @Test
    void testToString() {
    }
}