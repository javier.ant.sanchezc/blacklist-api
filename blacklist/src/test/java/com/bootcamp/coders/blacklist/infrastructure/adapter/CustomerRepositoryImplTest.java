package com.bootcamp.coders.blacklist.infrastructure.adapter;

import com.bootcamp.coders.blacklist.DatosMock;
import com.bootcamp.coders.blacklist.domain.model.customer.entity.Customer;
import com.bootcamp.coders.blacklist.domain.model.customer.repository.CustomerRepositoryJPA;
import com.bootcamp.coders.blacklist.domain.port.repository.CustomerRepository;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

class CustomerRepositoryImplTest {

    CustomerRepository  customerRepository;

    CustomerRepositoryJPA repositoryJPA;
    Customer cm;

    @BeforeEach
    void setUp() {
        customerRepository = mock(CustomerRepository.class);
     repositoryJPA = mock(CustomerRepositoryJPA.class);
        cm =new Customer();



    }

    @AfterEach
    void tearDown() {
    }

    @Test
    void searchIfBlacklisted() {
        Mockito.<Boolean>when(customerRepository.searchIfBlacklisted(cm)).thenReturn(DatosMock.cmc.equals(cm));
        assertEquals( repositoryJPA.customerIsBlackListedByName(cm.getFullName(),cm.getBirthDate()),repositoryJPA.customerIsBlackListedByName(DatosMock.cmc.getFullName(), DatosMock.cmc.getBirthDate()));
        assertEquals( repositoryJPA.customerIsBlackListedByDocument(cm.getDocumentType(),cm.getDocument(),cm.getDocumentIssueDate()),repositoryJPA.customerIsBlackListedByDocument(DatosMock.cmc.getDocumentType(), DatosMock.cmc.getDocument(),DatosMock.cmc.getDocumentIssueDate()));
        assertEquals( repositoryJPA.customerIsBlackListedByEmail(cm.getEmail()),repositoryJPA.customerIsBlackListedByEmail(DatosMock.cmc.getEmail()));
        assertEquals( repositoryJPA.customerIsBlackListedByCellphone(cm.getCellphone()),repositoryJPA.customerIsBlackListedByCellphone(DatosMock.cmc.getCellphone()));



    }

    @Test
    void test2() {
        boolean c1 = repositoryJPA.customerIsBlackListedByCellphone("1234546");
        boolean c2 = repositoryJPA.customerIsBlackListedByCellphone("1234546");

        assertEquals(c1,c2);

    }
}