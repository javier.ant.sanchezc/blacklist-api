package com.bootcamp.coders.blacklist.domain.model.customer.entity;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import java.time.LocalDate;


import static org.junit.jupiter.api.Assertions.*;

class CustomerTest {
    private Customer customer;
    final LocalDate LOCAL_DATE = LocalDate.of(2019, 12, 17);
    @BeforeEach
    void setUp() {

        customer = new Customer();
        customer.setEmail("correo@correo.com");
        customer.setDocument("123456789");
        customer.setFullName("name");
        customer.setCellphone("3156770860");
        customer.setDocumentIssueDate(LOCAL_DATE);
        customer.setBirthDate(LOCAL_DATE);





    }

    @AfterEach
    void tearDown() {
    }

    @Test

  void Customertest(){
        customer= new Customer("CC","11111","pepito@correo.com","javiersanchez","3156770860",LOCAL_DATE,LOCAL_DATE);

}


    @Test
    void getDocumentType() {
    }

    @Test
    void getDocument() {

        assertEquals(customer.getDocument(),"123456789");
    }

    @Test
    void getEmail() {

        assertEquals(customer.getEmail(),"correo@correo.com");
    }

    @Test
    @DisplayName("valida los nombre")
    void getFullName() {
        customer.setFullName("name");
        assertEquals(customer.getFullName(),"name");

    }

    @Test
    void getCellphone() {

        assertEquals(customer.getCellphone(),"3156770860");

    }

    @Test
    void getBirthDate() {

assertEquals(customer.getBirthDate(),LOCAL_DATE);
    }

    @Test
    void getDocumentIssueDate() {
        assertEquals(customer.getDocumentIssueDate(),LOCAL_DATE);


    }


}