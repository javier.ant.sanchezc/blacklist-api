package com.bootcamp.coders.blacklist.domain.model.customer.repository;

import com.bootcamp.coders.blacklist.DatosMock;
import com.bootcamp.coders.blacklist.domain.model.customer.entity.Customer;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.util.Assert;

import java.time.LocalDate;

import static org.junit.jupiter.api.Assertions.*;
import static org.junit.jupiter.api.Assertions.assertNotNull;

@DataJpaTest
class CustomerRepositoryJPATest {
    @Autowired
    CustomerRepositoryJPA  jpa;
    Customer cm;
    DatosMock mk;
    static   final LocalDate LOCAL_DATE = LocalDate.of(2011, 9, 02);

    @BeforeEach
    void setUp() {
        mk = new DatosMock();
        cm = new Customer();
        cm.setDocument("10974003");
        cm.setDocumentType("CC");
        cm.setFullName("Felipe Buitrago");
        cm.setEmail("afbuitragof@uq.com");
        cm.setCellphone("3194962307");
        cm.setDocumentIssueDate(LOCAL_DATE);
        cm.setBirthDate(LOCAL_DATE);




    }

    @AfterEach
    void tearDown() {
    }

    @Test
    void customerIsBlackListedByDocument() {
    boolean doc=  jpa.customerIsBlackListedByDocument(cm.getDocumentType(),cm.getDocument(), cm.getDocumentIssueDate());
    assertTrue(doc);
    assertNotNull(jpa.customerIsBlackListedByDocument(cm.getDocumentType(),cm.getDocument(), cm.getDocumentIssueDate()));

    }

    @Test
    void customerIsBlackListedByName() {
        boolean doc=  jpa.customerIsBlackListedByName(cm.getFullName(), cm.getBirthDate());
        assertFalse(doc);



    }

    @Test
    void customerIsBlackListedByEmail() {
        boolean doc=  jpa.customerIsBlackListedByEmail(cm.getEmail());
        assertTrue(doc);
    }

    @Test
    void customerIsBlackListedByCellphone() {
        boolean doc=  jpa.customerIsBlackListedByCellphone(cm.getCellphone());
        assertTrue(doc);

    }
}