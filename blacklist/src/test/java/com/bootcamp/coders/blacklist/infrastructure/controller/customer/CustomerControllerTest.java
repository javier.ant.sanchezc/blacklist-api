package com.bootcamp.coders.blacklist.infrastructure.controller.customer;

import com.bootcamp.coders.blacklist.DatosMock;
import com.bootcamp.coders.blacklist.application.service.customer.SearchBlacklistedApplicationService;
import com.bootcamp.coders.blacklist_openapiv3.project.api.model.CustomerInfo;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.test.web.servlet.MockMvc;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;

import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;


@WebMvcTest(CustomerController.class)
public class CustomerControllerTest {



    @Autowired
    private MockMvc mvc ;

    @MockBean
    private  CustomerInfo customerInfo;

    @MockBean
    private  SearchBlacklistedApplicationService service;


    @BeforeEach
    void setUp() {


    }

    @AfterEach
    void tearDown() {
    }

    @Test
    void consultBlackListed() throws Exception {
when(service.execute(customerInfo)).thenReturn(false);
mvc.perform(get("/blacklist/search").contentType(MediaType.APPLICATION_JSON))
        .andExpect(status().is4xxClientError());





    }
}