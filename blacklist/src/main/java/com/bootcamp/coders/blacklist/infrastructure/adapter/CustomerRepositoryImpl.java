package com.bootcamp.coders.blacklist.infrastructure.adapter;


import com.bootcamp.coders.blacklist.domain.model.customer.entity.Customer;
import com.bootcamp.coders.blacklist.domain.model.customer.repository.CustomerRepositoryJPA;
import com.bootcamp.coders.blacklist.domain.port.repository.CustomerRepository;
import org.springframework.stereotype.Repository;

@Repository
public class CustomerRepositoryImpl implements CustomerRepository {

    private final CustomerRepositoryJPA repositoryJPA;

    public CustomerRepositoryImpl(CustomerRepositoryJPA repositoryJPA) {
        this.repositoryJPA = repositoryJPA;
    }

    @Override
    public boolean searchIfBlacklisted(Customer customer) {

        if (this.repositoryJPA.customerIsBlackListedByName(customer.getFullName(), customer.getBirthDate())) {
            return true;
        }
        if (this.repositoryJPA.customerIsBlackListedByDocument(customer.getDocumentType(), customer.getDocument(), customer.getDocumentIssueDate())) {
            return true;
        }
        if (this.repositoryJPA.customerIsBlackListedByEmail(customer.getEmail())) {
            return true;
        }
        return this.repositoryJPA.customerIsBlackListedByCellphone(customer.getCellphone());
    }
}
