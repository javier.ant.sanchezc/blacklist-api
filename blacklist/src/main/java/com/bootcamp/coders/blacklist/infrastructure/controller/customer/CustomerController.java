package com.bootcamp.coders.blacklist.infrastructure.controller.customer;


import com.bootcamp.coders.blacklist.application.service.customer.SearchBlacklistedApplicationService;
import com.bootcamp.coders.blacklist_openapiv3.project.api.BlackListApi;
import com.bootcamp.coders.blacklist_openapiv3.project.api.model.BlackListResponse;
import com.bootcamp.coders.blacklist_openapiv3.project.api.model.BlackListResponseData;
import com.bootcamp.coders.blacklist_openapiv3.project.api.model.CustomerInfo;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class CustomerController implements BlackListApi {
    final static Log logger = LogFactory.getLog(CustomerController.class);
    private final SearchBlacklistedApplicationService service;

    public CustomerController(SearchBlacklistedApplicationService service)
    {
        this.service = service;
    }



    @Override

    public ResponseEntity<BlackListResponse> consultBlackListed(CustomerInfo customerInfo) {

        logger.info("--inicio de APP --");

        boolean isBlackListed = this.service.execute(customerInfo);

        BlackListResponse response = new BlackListResponse()
                .data(new BlackListResponseData().document(customerInfo.
                        getDocument()).blackListed(isBlackListed));

        logger.info("--fin de APP--");
        return new ResponseEntity<>(response, HttpStatus.OK);

    }



}
