package com.bootcamp.coders.blacklist.domain.model.customer.repository;


import com.bootcamp.coders.blacklist.domain.model.customer.entity.Customer;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.time.LocalDate;

public interface CustomerRepositoryJPA extends JpaRepository<Customer, Long> {

    @Query(nativeQuery = true,
            value = "SELECT CASE WHEN COUNT(1) > 0 THEN true ELSE false END FROM blacklisted_documents bld WHERE bld.document_type = ?1 AND bld.document = ?2 AND bld.document_issue_date = ?3")
    boolean customerIsBlackListedByDocument(String documentType, String document, LocalDate documentIssueDate);

    @Query(nativeQuery = true,
            value = "SELECT CASE WHEN COUNT(1) > 0 THEN true ELSE false END FROM blacklisted_documents bld WHERE bld.full_name = ?1 AND bld.birth_date = ?2")
    boolean customerIsBlackListedByName(String fullName, LocalDate birthDate);

    @Query(nativeQuery = true,
            value = "SELECT CASE WHEN COUNT(1) > 0 THEN true ELSE false END FROM blacklisted_documents bld WHERE bld.email = ?1")
    boolean customerIsBlackListedByEmail(String email);

    @Query(nativeQuery = true,
            value = "SELECT CASE WHEN COUNT(1) > 0 THEN true ELSE false END FROM blacklisted_documents bld WHERE bld.cellphone = ?1")
    boolean customerIsBlackListedByCellphone(String cellphone);
}
