package com.bootcamp.coders.blacklist.domain.port.repository;

import com.bootcamp.coders.blacklist.domain.model.customer.entity.Customer;

public interface CustomerRepository {
    boolean searchIfBlacklisted(Customer customer);
}
