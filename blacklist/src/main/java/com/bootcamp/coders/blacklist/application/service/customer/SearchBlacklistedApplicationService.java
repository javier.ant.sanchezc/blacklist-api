package com.bootcamp.coders.blacklist.application.service.customer;

import com.bootcamp.coders.blacklist.application.factory.CustomerFactory;
import com.bootcamp.coders.blacklist.domain.model.customer.entity.Customer;
import com.bootcamp.coders.blacklist.domain.service.SearchBlacklistedService;
import com.bootcamp.coders.blacklist_openapiv3.project.api.model.CustomerInfo;
import org.springframework.stereotype.Component;


@Component
public class SearchBlacklistedApplicationService {

    private final SearchBlacklistedService service;
    private final CustomerFactory factory;

    public SearchBlacklistedApplicationService(SearchBlacklistedService service, CustomerFactory factory) {
        this.factory = factory;
        this.service = service;
    }

    public boolean execute(CustomerInfo customerInfo) {
        Customer customer = this.factory.create(customerInfo);
        return this.service.searchIfBlacklisted(customer);
    }
}
