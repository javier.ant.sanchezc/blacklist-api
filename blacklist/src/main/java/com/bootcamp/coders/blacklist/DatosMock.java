package com.bootcamp.coders.blacklist;

import com.bootcamp.coders.blacklist.domain.model.customer.entity.Customer;
import liquibase.pro.packaged.C;
import liquibase.pro.packaged.S;

import java.time.LocalDate;
import java.util.Currency;

public class DatosMock {
    static   final LocalDate LOCAL_DATE = LocalDate.of(2011, 9, 02);
    public static  final Customer cmc = new Customer(
            "CC",
            "10974003",
            "",
            "",
            "",
            LOCAL_DATE,
            LOCAL_DATE );
}
