package com.bootcamp.coders.blacklist.domain.service;

import com.bootcamp.coders.blacklist.domain.model.customer.entity.Customer;
import com.bootcamp.coders.blacklist.domain.port.repository.CustomerRepository;
import org.springframework.stereotype.Service;

@Service
public class SearchBlacklistedService {

    private final CustomerRepository repository;

    public SearchBlacklistedService(CustomerRepository repository) {

        this.repository = repository;
    }

    public boolean searchIfBlacklisted(Customer customer) {

        return this.repository.searchIfBlacklisted(customer);

    }
}
