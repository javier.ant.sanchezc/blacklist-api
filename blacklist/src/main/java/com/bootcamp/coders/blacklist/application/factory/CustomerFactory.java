package com.bootcamp.coders.blacklist.application.factory;

import com.bootcamp.coders.blacklist.domain.model.customer.entity.Customer;
import com.bootcamp.coders.blacklist_openapiv3.project.api.model.CustomerInfo;
import org.springframework.stereotype.Component;

@Component
public class CustomerFactory {
    public Customer create(CustomerInfo customerInfo) {
        return new Customer(
                customerInfo.getDocumentType(),
                customerInfo.getDocument(),
                customerInfo.getEmail(),
                customerInfo.getName() + " " + customerInfo.getLastName(),
                customerInfo.getCellphone(),
                customerInfo.getBirthDate(),
                customerInfo.getDocumentIssueDate()
        );
    }
}
