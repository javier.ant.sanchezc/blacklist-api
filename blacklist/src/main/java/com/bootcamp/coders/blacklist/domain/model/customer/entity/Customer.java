package com.bootcamp.coders.blacklist.domain.model.customer.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.time.LocalDate;

@Entity
@Data
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "BLACKLISTED_DOCUMENTS")


public class Customer {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String documentType;
    private String document;
    private String email;
    private String fullName;
    private String cellphone;
    private LocalDate birthDate;
    private LocalDate documentIssueDate;

    public Customer(String documentType,
                    String document,
                    String email,
                    String fullName,
                    String cellphone,
                    LocalDate birthDate,
                    LocalDate documentIssueDate) {
        this.documentType = documentType;
        this.document = document;
        this.email = email;
        this.fullName = fullName;
        this.cellphone = cellphone;
        this.birthDate = birthDate;
        this.documentIssueDate = documentIssueDate;
    }


}
